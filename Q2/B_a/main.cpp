#include<iostream>
#include<cmath>
#include<iomanip>

float func(float a);

int main(){
  float xOld{0.01}, xNew{0}, xNow{1};
  float error{1};
  int i{0};
  while(error > 0.0001){
    //xNew=Xn+1, xNow=Xn, xOld=Xn-1
    xNew = (xOld * func(xNow) - xNow * func(xOld)) / (func(xNow) - func(xOld));
    error = std::abs((xNew - xNow) / xNow);
    std::cout << "n: "  << ++i << "\t";
    std::cout << "Xn+1: " << std::setw(2) << std::setprecision(4) << xNew << "\t";
    std::cout << "Xn: " << std::setw(4) << std::setprecision(4) << xNow << "\t" ;
    std::cout << "Error: " << std::setw(4) << std::setprecision(3) << error << std::setprecision(3) << "\t";
    std::cout << "f(Xn+1): " << std::setprecision(3) << func(xNew) << std::endl;
    xOld = xNow;
    xNow = xNew;
  };
  std::cout << "Xfinal: " << xNew << std::endl;
  return 0;
}

float func(float a){
  return (std::pow(a, 3) + std::pow(a, 2) + a + 1);
  //return (1/std::tanh(a)-std::log10(a));
};
