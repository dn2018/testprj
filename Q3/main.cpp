#include<iostream>
#include<string>
#include<sstream>

int arraySum(int a,int b,int* p){
  int k{}, sum{0};
  for(k=a;k<(b+1);k++)
    sum += (*(p + k));
  return sum;
}

int main(){
  int sizeArr{}, i{}, j{}, totalSum{0};
  //int counter{};
  std::string arrayNumStr;
  std::cout << "Enter size of arrays: ";
  std::cin >> sizeArr;
  //std::cout << "size is: " << sizeArr << std::endl;
  int* p{new int[sizeArr]};
  std::cout << "Enter your arrays: ";
  std::cin.ignore();
  std::getline(std::cin, arrayNumStr);
  std::istringstream iss{arrayNumStr}; //convert string to integer
  for(i=0;i<2*sizeArr;i++){
    /*if(arrayNumStr[i]!=' '){
      *(p + counter) = arrayNumStr[i] - 48;
      counter++;
      };*/
    iss >> *(p+i); //filling arrays
  };
  for(i=0;i<sizeArr;i++){
    for(j=i;j<sizeArr;j++)
      totalSum += arraySum(i, j, p);
    };
  delete[] p;
  std::cout << "result is: " << totalSum << std::endl;
  return 0;
}
